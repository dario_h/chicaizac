//
//  ViewControllerClimaChristian.swift
//  WeatherApp
//
//  Created by Christian on 29/5/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewControllerClimaChristian: UIViewController {
    
    
    
    @IBOutlet weak var textfieldCity: UITextField!
    
    

    @IBAction func buttonOk(_ sender: Any) {
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(textfieldCity.text ?? "Quito")&appid=bf606df721c5f3ab20b54d79ec506fd2"
        let url = URL (string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else{
                
            print("error no data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(ChristianWeatherInfo.self, from: data)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            print(weatherInfo.weather[0].description)
            
            DispatchQueue.main.async{
            self.labelClima.text = "\(weatherInfo.weather[0].description)"

            }
        }
        task.resume()
    }
    
    
    @IBOutlet weak var labelClima: UILabel!
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
