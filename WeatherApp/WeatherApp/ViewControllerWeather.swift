//
//  ViewControllerWeather.swift
//  WeatherApp
//
//  Created by Dario Herrera on 29/5/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewControllerWeather: UIViewController {
    
    
    
    @IBOutlet weak var txtResultadoClima: UILabel!
    
    
    @IBOutlet weak var txtCiudad: UITextField!
    
    
    @IBAction func botonConsultarClimaCiudad(_ sender: Any) {
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(txtCiudad.text ?? "quito")&appid=bf79c6246cd574b350bc52b872c68605"
        
        let url = URL (string: urlString)
        
        let sesion = URLSession.shared
        
        let task = sesion.dataTask(with: url!) { (data, Response, Error) in
            
            guard let data = data else{
                print("Error NO data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(DarioWeatherInfo.self, from: data) else{
                print("Error decoding Weather")
                return
            }
            
            //print(weatherInfo.weather[0].description)
            
            //este metodo es para que devuelva al instante el resultado al hilo principal
            DispatchQueue.main.async {
                self.txtResultadoClima.text = "\(weatherInfo.weather[0].description)"
            }
            
            
        }
        task.resume()
        
    }

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}
