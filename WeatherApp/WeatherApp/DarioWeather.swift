//
//  DarioWeather.swift
//  WeatherApp
//
//  Created by Dario Herrera on 30/5/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import Foundation


///en caso de que el nombre del json seaa de la siguiente
/*
"main_wather":"rain"

enum CodingKeys: String, CodingKeys {
    case weather
    case mainWeather
}
 */

struct DarioWeatherInfo: Decodable {
    
    //json campo weather
    let weather: [DarioWeather]
    
    
  
}


struct DarioWeather: Decodable {
    //obtengo los objetos del arreglo
    let id: Int
    let description: String
}
