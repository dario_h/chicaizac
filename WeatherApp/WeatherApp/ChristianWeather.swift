//
//  ChristianWeather.swift
//  WeatherApp
//
//  Created by Christian on 30/5/18.
//  Copyright © 2018 Christian. All rights reserved.
//
/*
 let mainWeather:String
 
 enum weather
 case mainWeather = "main_weather"*/


import Foundation

struct ChristianWeatherInfo: Decodable{
    let weather: [ChristianWeather]

    
}

struct ChristianWeather: Decodable{
    let id:Int
    let description:String
    
}
